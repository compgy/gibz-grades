FROM python:3.10.7-slim-bullseye

RUN pip install poetry

WORKDIR /app

COPY poetry.lock pyproject.toml ./

RUN poetry config virtualenvs.create false \
  && poetry install --no-dev --no-root

COPY . .

WORKDIR /app/gibz_grades

CMD ["python", "main.py"]
