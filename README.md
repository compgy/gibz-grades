# GIBZ Grades 📓
![](https://media.giphy.com/media/c5skRQb3BXp8RwKGKW/giphy.gif)

> This Python script is designed to scrape the "Schulnetz Mobile" portal for new
> grades and store them in a JSON file. Additionally, it will send a Telegram
> message if a new grade is uploaded. To use the script, it is meant to be run
> periodically using a cron job. This allows for automated checking of new grades
> and eliminates the need to manually check for updates.

## Installation Instructions

### Prerequisites
Schulnetz Mobile has to be enabled and set up. Head to <https://gibz.zg.ch/> to
enable it, set a pin and then come back.

### Setup

#### Locally 💻

* Copy the env.example file and rename it to `.env`
* Change the `PIN` and `URL` variables to your own
* Install the python dependencies with `poetry install`
* Setup the Telegram Bot by running `poetry run telegram-send --configure`
* Run the program with `poetry run python gibz_grades/main.py`

#### Docker 🐳

The Docker image has to be built manually, since a Telegram bot needs to be set
up first.
##### Bot Setup 🤖
* Install the python dependencies with `poetry install`
* Setup the Telegram Bot by running `telegram-send --configure --config ./telegram-send.conf` in the project root.

##### Docker Image 💾
* Build the docker image with: `docker build -t YOUR_IMAGE_NAME .`
* Run the image and set the environment variables with: `docker run -e PIN=1234 -e URL=http://example.com YOUR_IMAGE_NAME`

## Usage 
This script is meant to be ran periodically. For stability reasons a scheduler
is not included in the python code. Instead, the periodic execution should be
managed by the operating system. For Linux I suggest using `cron`; Windows, idfk.

### Crontab Example 🕑

* Type `crontab -e` to open the crontab file in a text editor.
* At the bottom of the file, add the following line: `0 */60 * * * docker run -e PIN=1234 -e URL=http://example.com YOUR_IMAGE_NAME`
* Save the file and exit the text editor.

*Important Note:* Don't overdo it with the frequency! Running the script each hour is perfectly sufficient. I'm not responsible for any damage done to GIBZ Servers.💀
## Contributions
If you'd like to contribute, please go ahead. You can fork the project and file
a merge request.

## License
This Project is realease under the MIT License.