import hashlib
import os
from bs4 import BeautifulSoup
import telegram_send
import requests

import tinydb
from dotenv import load_dotenv

load_dotenv("../.env")

def get_html():
    """Get HTML content from URL."""
    return requests.post(
        os.getenv("URL"),
        data={
            "pin": os.getenv("PIN"),
        },
    ).text


def get_table(soup):
    """Get table from HTML."""
    return soup.find("table")


def get_table_rows(table):
    """Get rows from table."""
    return table.find_all("tr")


def parse_table_row(row):
    row_items = row.find_all("td")
    if row_items[0].find("span") != None:
        raise Exception("No grades yet")

    return {
        "subject": row_items[0].text,
        "exam": row_items[1].text,
        "date": row_items[2].text,
        "grade": row_items[3].text,
    }


def get_grades():
    try:
        soup = BeautifulSoup(get_html(), "html.parser")
        table = get_table(soup)
        rows = get_table_rows(table)
        grades = list(map(parse_table_row, rows))
        save_grades(grades)
        return grades
    except Exception as e:
        print(e)


def save_grades(grades):
    db = tinydb.TinyDB("grades.json")
    # hash every dictionary and add to hash key to easily find duplicate grades
    grades = [
        {"hash": hashlib.sha1(str(grade).encode("utf-8")).hexdigest(), **grade}
        for grade in grades
    ]
    for grade in grades:
        # if there's a new grade add to db and send telegram message
        if not db.search(tinydb.Query().hash == grade["hash"]):
            db.insert(grade)
            send_grade(grade)


def send_grade(grade):
    telegram_send.send(
        messages=[
            f"{grade['subject']} \n {grade['exam']} \n {grade['date']} \n {grade['grade']}"
        ], conf="../telegram-send.conf"
    )

def send_message(message):
    telegram_send.send(
        messages=[
            message
        ], conf="../telegram-send.conf"
    )

def main() -> None:

    """Start the bot."""
    get_grades()


if __name__ == "__main__":
    main()
